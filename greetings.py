from nameko.rpc import rpc
from functional import seq
import zlib
from nameko.testing.services import worker_factory


def encode_it(string):
    return zlib.compress(string.encode('utf8'))


def decode_it(string):
    return zlib.decompress(string).decode('utf8')


class GreetingService:
    name = "greeting_service"

    @rpc
    def square_my_odds(self, integers: int) -> list:
        return seq(integers).filter(lambda x: x % 2 != 0).map(lambda x: x**2).to_list()

    @rpc
    def compress_my_strings(self, strings: list) -> dict:
        return seq(strings).map(lambda string: (string, encode_it(string))).to_dict()

    @rpc
    def decompress_my_string(self, string: str) -> str:
        return str(decode_it(string))


def test_greeting_service():
    service = worker_factory(GreetingService)
    assert service.square_my_odds([1,2,3,4,5]) == [1, 9, 25]
    compressed = service.compress_my_strings(["hi_there", "this_is_cool"])
    assert service.decompress_my_string(compressed.get('hi_there')) == 'hi_there'
