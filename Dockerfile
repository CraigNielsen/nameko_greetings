FROM python:3.7.6-slim-buster
WORKDIR /usr/local/greetings
COPY . .
RUN pip install -r requirements.txt
RUN pytest greetings.py
CMD nameko run --config config.yml greetings
