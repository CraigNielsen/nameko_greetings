# To get started
``./do_everything.sh.``


If you want a single command to do all the things, make sure you are in a safe python env (pyenv) and run ^^

This will install requirements locally, build the image, deploy the services to docker and run shell commands locally that will connect via the exposed rabbit port in docker. The decompress function is not called however.
If the last step fails, rabbit may not have been ready.. wait a few seconds and run ``nameko shell --config local_config.yml < commands.txt``
___

### Git

A quick note on git. I generally squash commits into features, and have done so for this assignment. I was already quite deep into the feature by the time I made my first commit, so I just squashed it anyway.
___

## Thoughts
I really enjoyed learning about and using nameko in this task. It can save tons of time by abstracting away some network details as well as quick and easy service lookup. The rpc functionality is a big win (alot of my thoughts on RPC echo this: https://www.smashingmagazine.com/2016/09/understanding-rest-and-rpc-for-http-apis/ ), basically for actions over basic api crud, rpc just makes more sense.

Of course one should make sure to use the right tool for the job, and for nameko this would not include being public facing api. As a microservice tool, it is really quick to get going, and it is concise; maintaining more concise code is always more enjoyable.
It would be interesting to see how the microservices handle retries, downstream failures, and if there are any neat easy wins for metrics.

## Time Spent and workflow

#### Nameko
Took about 5 minutes to get going with a basic shell call after dependencies were up (I used rpc).
I started with the functions, added some tests, and tweaked until I was happy. In total about 15 minutes here.
#### Docker
This part did take longer than I expected. It took about an hour to build and run docker images including some network troubleshooting around nameko hostname service lookups. I found how to set the hostname for service lookups in the config file, and then moved on.
#### Compression
This probably took the longest strangely enough, about 2 hours.
I left the compression as a final thought, and then struggled to find a simple lib that did the compression; I eventually used zlib.
Then faced an issue where the serialization of a bytes string failed, of course. Nameko uses json as a default serializer. I did some more digging and found an option to pickle at serialization. I set more configs, rebuilt, and all good.
The pickling is RAM hungry, so more efficient solutions might need to be explored if this needs to scale, but I wouldn't prematurely optimize. Load tests can help identify other bottlenecks as well.

___

## Design
#### File structure
My approach to the task was to create a friendly hello world type app, so there are no configs based on environment, or config directories. Everything is in a single folder, with files for docker environments, and local environemnts.
In an app that will be long term, moving configs to environment and nesting directories for testing, config etc will be required.

#### The General Code Approach
In terms of design decisions, I always favor pure functional code, and find class state can introduce bugs. I like the pyfunctional library (I like the readability of the function defs over the standard python map, reduce, filter. Especially in larger data pipelines).


I often choose to use functions and modules over classes, since I prefer stateless services, but often OO suites the use case better.
The encoding and decoding functions could move to another file, but I kept eveything quite rudimentary.

