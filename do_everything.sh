pip install -r local_py_requirements.txt
./build.sh
docker-compose up -d
echo "waiting for rabbitmq to wake up 5 seconds"
sleep 5
nameko shell --config local_config.yml < commands.txt
